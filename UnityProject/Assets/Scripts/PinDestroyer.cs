﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinDestroyer : MonoBehaviour
{

    public WhipScript player;
    public CursorScript cusrsor;

    void Update()
    {

        if (Vector3.Distance(player.hitPointRight, transform.position) <= 0.5)
        {
            StartCoroutine(Wait(3, SelfdestroyRight));
        }
        if (Vector3.Distance(player.hitPointLeft, transform.position) <= 0.5)
        {
            StartCoroutine(Wait(3, SelfdestroyLeft));
        }
    }

    IEnumerator Wait(float seconds, Func<bool> doSomething)
    {
        yield return new WaitForSeconds(seconds);
        doSomething();
    }

    bool SelfdestroyRight()
    {
        player.hitPointRight = Vector3.zero;

        cusrsor.right.transform.SetParent(cusrsor.transform);
        cusrsor.right.position = new Vector3(cusrsor.transform.position.x + 16f, cusrsor.transform.position.y, cusrsor.transform.position.z);

        Destroy(gameObject);

        return true;
    }

    bool SelfdestroyLeft()
    {
        player.hitPointLeft = Vector3.zero;

        cusrsor.left.transform.SetParent(cusrsor.transform);
        cusrsor.left.position = new Vector3(cusrsor.transform.position.x - 16f, cusrsor.transform.position.y, cusrsor.transform.position.z);

        Destroy(gameObject);

        return true;
    }

}
