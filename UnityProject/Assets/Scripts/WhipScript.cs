﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhipScript : MonoBehaviour
{

    public GameObject player;
    public float distance = 4f;
    public float speed = 2f;
    public Vector3 hitPointLeft = Vector3.zero;
    public Vector3 hitPointRight = Vector3.zero;
    public RectTransform leftCursor;
    public RectTransform rightCursor;
    public Dictionary<string, Func<RaycastHit, string, bool>> actions;

    public LayerMask whipMask;
    // Use this for initialization
    void Start()
    {
        actions = new Dictionary<string, Func<RaycastHit, string, bool>>();
        actions.Add("Pin", GoToPin);
        actions.Add("Lever", UseLever);
        actions.Add("Torch", LightTorch);
    }

    // Update is called once per frame
    void Update()
    {
        if (hitPointLeft != Vector3.zero)
        {
            player.GetComponent<Rigidbody>().AddForce((hitPointLeft - player.transform.position) * speed, ForceMode.Impulse);
            player.GetComponent<Rigidbody>().velocity = player.GetComponent<Rigidbody>().velocity.normalized;
        }
        if (hitPointRight != Vector3.zero)
        {
            player.GetComponent<Rigidbody>().AddForce((hitPointRight - player.transform.position) * speed, ForceMode.Impulse);
            player.GetComponent<Rigidbody>().velocity = player.GetComponent<Rigidbody>().velocity.normalized;
        }


        if (Input.GetMouseButtonUp(0))
        {
            hitPointLeft = Vector3.zero;
        }
        if (Input.GetMouseButtonUp(1))
        {
            hitPointRight = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(leftCursor.position);
            if (Physics.Raycast(ray, out hit, 100.0f, whipMask))
            {
                if (Vector3.Distance(hit.point, player.transform.position) <= distance)
                {
                    try { actions[hit.transform.tag](hit, "Left"); }
                    catch { }
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(rightCursor.position);
            if (Physics.Raycast(ray, out hit, 100.0f, whipMask))
            {
                if (Vector3.Distance(hit.point, player.transform.position) <= distance)
                {
                    try { actions[hit.transform.tag](hit, "Right"); }
                    catch { }
                }
            }
        }
    }

    private bool checkHit(Vector3 firstPoint, Vector3 secondPoint, string tag)
    {
        RaycastHit hit;
        Ray ray = new Ray(firstPoint, (secondPoint - firstPoint));
        if (Physics.Raycast(ray, out hit, 100.0f, whipMask))
        {
            if (hit.transform.tag == tag)
                return true;
        }
        return false;
    }

    private bool GoToPin(RaycastHit hit, string direction)
    {
        if (checkHit(player.transform.position, hit.point, hit.transform.tag))
        {
            if (direction == "Left")
            {
                hitPointLeft = hit.point;
            }
            else if (direction == "Right")
            {
                hitPointRight = hit.point;
            }
        }
        return true;
    }

    private bool UseLever(RaycastHit hit, string direction)
    {
        if (checkHit(player.transform.position, hit.point, hit.transform.tag))
        {
            var lever = hit.transform.parent;
            if (!lever.GetComponent<LeverScript>().isEnabled)
            {
                lever.GetComponent<LeverScript>().isEnabled = true;
                lever.GetComponent<LeverScript>().gateSpeed = -2;
                lever.GetComponentsInChildren<Transform>()[2].Rotate(90, 0, 0);
            }
            else
            {
                lever.GetComponent<LeverScript>().isEnabled = false;
                lever.GetComponent<LeverScript>().gateSpeed = 2;
                lever.GetComponentsInChildren<Transform>()[2].Rotate(-90, 0, 0);
            }
        }
        return true;
    }

    private bool LightTorch(RaycastHit hit, string direction)
    {
        if (checkHit(player.transform.position, hit.point, hit.transform.tag))
        {
            var torch = hit.transform.parent;
            torch.GetComponentsInChildren<Transform>()[1].gameObject.tag = "Untagged";
            torch.GetComponentsInChildren<Transform>()[2].gameObject.tag = "Untagged";
            torch.GetComponentsInChildren<Transform>(true)[1].gameObject.SetActive(true);
        }
        return true;
    }
}
