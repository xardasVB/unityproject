﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorScript : MonoBehaviour {

    public Canvas canvas;
    public Transform left;
    public Transform right;
    public LayerMask whipMask;

    WhipScript whip;
    List<string> tags;

    // Use this for initialization
    void Start ()
    {
        whip = GameObject.FindGameObjectWithTag("Player").GetComponent<WhipScript>();
        tags = new List<string>(whip.actions.Keys);
        right = GetComponentsInChildren<Transform>()[3];
        left = GetComponentsInChildren<Transform>()[2];

        //right.GetComponent<Image>().sprite = Resources.Load<Sprite>("ActiveWhip");
    }
	
	// Update is called once per frame
	void Update ()
    {
        Cursor.visible = false;
        transform.position = Input.mousePosition;


        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(left.position);
        if (Physics.Raycast(ray, out hit, 100.0f, whipMask))
        {
            if (tags.Contains(hit.transform.tag) && whip.hitPointLeft == Vector3.zero)
                left.GetComponent<Image>().sprite = Resources.Load<Sprite>("ActiveWhip");
            else
                left.GetComponent<Image>().sprite = Resources.Load<Sprite>("InactiveWhip");
        }

        ray = Camera.main.ScreenPointToRay(right.position);
        if (Physics.Raycast(ray, out hit, 100.0f, whipMask))
        {
            if (tags.Contains(hit.transform.tag) && whip.hitPointRight == Vector3.zero)
                right.GetComponent<Image>().sprite = Resources.Load<Sprite>("ActiveWhip");
            else
                right.GetComponent<Image>().sprite = Resources.Load<Sprite>("InactiveWhip");
        }

        if (whip.hitPointLeft != Vector3.zero)
            left.position = Camera.main.WorldToScreenPoint(whip.hitPointLeft);
        if (whip.hitPointRight != Vector3.zero)
            right.position = Camera.main.WorldToScreenPoint(whip.hitPointRight);

        if (Input.GetMouseButtonDown(0) && whip.hitPointLeft != Vector3.zero)
        {
            left.transform.SetParent(canvas.transform);
        }
        if (Input.GetMouseButtonUp(0))
        {
            left.transform.SetParent(transform);
            left.position = new Vector3(transform.position.x - 16f, transform.position.y, transform.position.z);
        }


        if (Input.GetMouseButtonDown(1) && whip.hitPointRight != Vector3.zero)
        {
            right.transform.SetParent(canvas.transform);
        }
        if (Input.GetMouseButtonUp(1))
        {
            right.transform.SetParent(transform);
            right.position = new Vector3(transform.position.x + 16f, transform.position.y, transform.position.z);
        }
    }
}
