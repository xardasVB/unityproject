﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanScript : MonoBehaviour {

    public bool fanEnabled;
    public float force;
    public List<Collider> objects;

	// Use this for initialization
	void Start () {
        fanEnabled = true;
        force = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (fanEnabled)
        {
            transform.Rotate(0, 0, 30);
            foreach (var obj in objects)
                obj.GetComponent<Rigidbody>().AddForce(transform.forward * force * -1, ForceMode.Impulse);
        }
	}
}
