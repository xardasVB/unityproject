﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public GameObject gate;
    private int gateSpeed = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        foreach (var obstacle in gate.GetComponentsInChildren<Transform>())
        {
            if (obstacle.tag == "Obstacle")
            {
                obstacle.Translate(Vector3.up * gateSpeed * Time.deltaTime);
                
                if (obstacle.localPosition.y > 0)
                {
                    gateSpeed = 0;
                    obstacle.localPosition = new Vector3(obstacle.localPosition.x, 0, obstacle.localPosition.z);
                }

                if (obstacle.localPosition.y < (obstacle.localScale.y + 1f) * -2)
                {
                    gateSpeed = 0;
                    obstacle.localPosition = new Vector3(obstacle.localPosition.x, (obstacle.localScale.y + 1f) * -2 , obstacle.localPosition.z);
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 0.02f, transform.position.z);
        gateSpeed = -2;
    }

    private void OnCollisionExit(Collision collision)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 0.02f, transform.position.z);
        gateSpeed = 2;
    }
}
