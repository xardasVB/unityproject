﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TorchBasic : MonoBehaviour {

    public Light LightSrc;

	// Use this for initialization
	void Start () {
	    LightSrc.DOIntensity(1.72f, 3.0f).SetLoops(-1, LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
