﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    Transform target;

	// Use this for initialization
	void Start ()
    {
        target = GameObject.FindWithTag("Target").transform;
        transform.LookAt(target);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Vector3.Distance(transform.position, target.position) >= 1f && Vector3.Distance(transform.position, target.position) <= 13f)
            transform.Translate(Vector3.forward * Input.GetAxis("Mouse ScrollWheel") * 5f);

        if (Vector3.Distance(transform.position, target.position) > 13f)
            transform.Translate(Vector3.forward * 0.1f);
        if (Vector3.Distance(transform.position, target.position) < 1f)
            transform.Translate(Vector3.forward * -0.1f);

        if (Input.GetKey(KeyCode.Q))
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * 10);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * -10);
        }


    }
}
