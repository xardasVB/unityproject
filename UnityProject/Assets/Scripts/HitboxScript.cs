﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxScript : MonoBehaviour {
    public FanScript parent;
    
    private void OnTriggerEnter(Collider other)
    {
        parent.objects.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        parent.objects.Remove(other);
    }
}
